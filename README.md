# NodeJS Challenges - Object-Oriented Programming 


## Objectives:


- To know about basic concepts of `Object-oriented` Programming.
- To know about `OOP` concepts in JavaScript.

### Theory & Documentation

# Objects

`Objects` are the basic run-time bodies in an object-oriented framework. They may represent a place, a person, an account, a table of data, or anything that the program needs to handle. Objects can also represent user-defined data such as vectors, time, and lists.

Consider two objects, `customer` and `account` in a program. The `customer` object may send a message requesting the bank balance. 

## Classes

We know that objects hold the data and the functions to manipulate the data. However, the two can be bound together in a user-defined data type with the help of classes. Any number of objects can be created in a class. Each object is associated with the data of type class. A class is therefore a collection of objects of similar types. 

For example, consider the class `Fruit`. 

We can create multiple objects for this class `Fruit`:

- `mango`,
- `banana`,
- `apple`...

This will create an object `mango` or `banana` or `apple` belonging to the class `fruit`.  

## Encapsulation

`Encapsulation` is the wrapping up/binding of data and function into a single unit called `class`. Data encapsulation is the most prominent feature of a class wherein the data is not accessible to the outside world, and only those functions wrapped inside the class can access it. These functions serve as the interface between the object’s data and the program. 

## Inheritance

The phenomenon where objects of one class acquire the properties of objects of another class is called `Inheritance`. It supports the concept of hierarchical classification. Consider the object `car` that falls in the class `Vehicles` and `Light Weight Vehicles`.

In OOP, the concept of inheritance ensures reusability. This means that additional features can be added to an existing class without modifying it. This is made possible by deriving a new class from the existing one. 


## OOP Concepts in JavaScript

How these concepts are implemented in Javascript:

### Object

An `Object` is a unique entity which contains property and methods. For example `car` is a real life Object, which have some characteristics like color, type, model, horsepower and performs certain action like drive. 

The `characteristics` of an Object are called as `Property` in Object Oriented Programming and the `actions` are called `methods`. 

An `Object` is an `instance` of a class. Objects are everywhere in JavaScript almost every element is an Object whether it is a function, arrays and string.

> A Method in javascript is a property of an object whose value is a function. 

An `Object` can be created in three ways in JavaScript:

- Using an Object Literal:

```javascript
//Defining object
let person = {
    first_name:'Roger',
    last_name: 'Berk',
  
    //method
    getFunction : function(){
        return (`The name of the person is ${person.first_name} ${person.last_name}`)
    },
    //object within object
    phone_number : {
        mobile:'23456',
        ext:'9010'
    }
}

console.log(person.getFunction())
console.log(person.phone_number.ext)
```

- Using an Object Constructor (`constructor function`): 

```javascript
//using a constructor
  function person(first_name,last_name){
     this.first_name = first_name;
     this.last_name = last_name;
  }
  //creating new instances of person object
  let person1 = new person('Roger','Marks');
  let person2 = new person('Roy','Alverad');
    
  console.log(person1.first_name);
  console.log(`${person2.first_name} ${person2.last_name}`);
```

- Using Object.create() method: 

The `Object.create()` method creates a new object, using an existing object as the prototype of the newly created object.

```javascript
const coder = {
    isStudying : false,
    printIntroduction : function(){
        console.log(`My name is ${this.name}. Am I 
          studying?: ${this.isStudying}.`)
    }
}
// Object.create() method
const me = Object.create(coder);
  
// "name" is a property set on "me", but not on "coder"
me.name = 'Roy'; 
  
// Inherited properties can be overwritten
me.isStudying = true; 
  
me.printIntroduction()
```

### Classes

`Classes` are blueprint of an Object. A `class` can have many Objects, because class is a template while Object are instances of the class or the concrete implementation. 

Before we move further into implementation, we should know unlike other Object Oriented Language there is `no classes` in JavaScript we have only `Object`. 

JavaScript is a `prototype` based object oriented language, which means it doesn’t have classes rather it define behaviors using `constructor function` and then reuse it using the `prototype`. 

> Even the `classes` provided by ECMA2015 are `objects`.

"JavaScript classes, introduced in ECMAScript 2015, are primarily syntactical sugar over JavaScript’s existing prototype-based inheritance. The class syntax is not introducing a new object-oriented inheritance model to JavaScript. JavaScript classes provide a much simpler and clearer syntax to create objects and deal with inheritance". [-Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)


#### Class Example

- Using `ES6`:

Let's use `ES6` syntax to define an `Object` and simulate them as classes.

```javascript
class Cat {
    constructor (name) {
      this.name = name
    }
  }

const tosti = new Cat('Tosti')

console.log(tosti.name)  // 'Tosti'
```

- Using a `constructor function` (javascript traditional way):

```javascript
function Cat (name) {
    this.name = name
}

const tosti = new Cat('Tosti')

console.log(tosti.name)  // Tosti
```

### Methods

#### Prototype Methods

`Prototype methods` are methods that are attached to instances of a class. 

- With constructor functions, we would have to modify the prototype of our 
  function directly.


```javascript
// Using a constructor function (javascript traditional way)
function Vehicle(name, maker, engine){
    this.name = name,
    this.maker = maker,
    this.engine = engine
}
  
Vehicle.prototype.getDetails = function(){
    return `The name of the bike is ${this.name}.`
}
  
let bike1 = new Vehicle('Hayabusa','Suzuki','45tyu')
let bike2 = new Vehicle('Ninja','Kawasaki','8700j')
  
console.log(bike1.name)
console.log(bike2.maker)
console.log(bike1.getDetails())
```

Output is...

```
Hayabusa
Kawasaki
The name of the bike is Hayabusa.
```

- With ES6 classes, we can add the method definition directly in the class 
  declaration.

```javascript
// Defining class using es6
class Vehicle {
    constructor(name, maker, engine) {
      this.name = name
      this.maker =  maker
      this.engine = engine
    }
  
    getDetails(){
        return `The name of the bike is ${this.name}.`
    }
}
  
// Making object with the help of the constructor
let bike1 = new Vehicle('Hayabusa', 'Suzuki', '567pl')
let bike2 = new Vehicle('Ninja', 'Kawasaki', '876yh')
  
console.log(bike1.name)    
console.log(bike2.maker)   
console.log(bike1.getDetails())
```

Output is...

```
Hayabusa
Kawasaki
The name of the bike is Hayabusa.
```

#### Static Methods

`Static methods` are methods which are added to the class itself and are not attached to instances of the class.

```javascript
class Cat {
    constructor (name) {
        this.name = name;
    }

    static isCat (animal) {
        return Object.getPrototypeOf(animal).constructor.name === this.name;
    }
}

var toto = new Cat('Toto');

toto.isCat        // undefined

Cat.isCat(toto)   // true
```

### Omitting the new Operator

What happens if you inadvertently invoke a `constructor function` without using the new operator?

```javascript
function Cat (name) {
    this.name = name
}

// missing new keyword
var tosti = Cat('Tostivaca')


console.log(tosti)          // undefined 

// window
console.log(window.name)    // 'Tostivaca'

// Equivalent of window in nodeJS
console.log(global.name)    // 'Tostivaca' 
```

What's going on? Without using the `new` operator, no object was created. The function was invoked just like any other regular function and adds the properties to the global object–in browsers this would be `window`. 

Since the function doesn't return anything (except `undefined`, which all functions return by default), the `tosti` variable ended up being assigned to `undefined`.

One feature of `ES6 classes` is the constructor function will only be invoked when using the `new` keyword.

```javascript
class Cat {
    constructor (name) {
        this.name = name
    }
}

var toto = Cat('Toto')  // Class constructor Cat cannot be invoked without 'new'
```

> If a `constructor function` is called with the `new` operator, the value of `this` is set to the newly-created object. If a `method` is invoked on an object, `this` is set to that object itself. And if a `function` is simply invoked, `this` is set to the global object: `window`.


### Encapsulation

The process of wrapping `property` and `function` within a single unit is known as encapsulation.

#### Encapsulation example 1

```javascript
class Person{
    constructor(name,id){
        this.name = name
        this.id = id
    }

    add_Address(add){
        this.add = add
    }
    
    getDetails(){
        console.log(`Name is ${this.name}, Address is: ${this.add}`)
    }
}
  
let person1 = new Person('Roy', 33)

person1.add_Address('CDMX')
person1.getDetails()
```

Output is...

```
Name is Roy, Address is: CDMX
```

Sometimes encapsulation refers to `hiding of data` or `data Abstraction` which means representing essential features hiding the background detail. 

Most of the OOP languages provide access modifiers to restrict the scope of a variable, but there are no such access modifiers in JavaScript but there are certain way by which we can restrict the scope of variable within the Class/Object.

> To do this, we can utilize `block scoping` using `const` or `let` within the `class constructor` or `constructor function`, and provide public methods that can return or operate on these `encapsulated` variables.


```javascript
function Person(fullName, num) {
    let name = fullName
    let id = num

    this.add_Address = function(add) {
        this.add = add
    }

    let add_Address2 = function(add2) {
        this.add2 = add2
    }

    this.getDetails = function(){
        console.log(`Name is ${name}, Address is: ${this.add} and my Address2 is ${this.add2}` )
    }
}
  
let person1 = new Person('Roy', 33)

person1.add_Address('CDMX')         //Set Address     
person1.getDetails()                //Name is Roy, Address is: CDMX and my Address2 is undefined
console.log(person1.name)           //undefined
person1.add_Address2('Germany')     //TypeError: person1.add_Address2 is not a function
```

In the above example we try to access some property `person1.name` and functions `person1.add_Address2()` but it returns undefined and also an error, respectively, while there is a method `person1.add_Address()` which we can access it, by changing the way to define a function we can restrict its scope.

#### Encapsulation example 2

For this example, let's say we're making a `Counter` component. This will be a reusable component that encapsulates a `count` and allows the user to increase or decrease the counter linearly. The user should be able to look at the count variable, but they shouldn't be able to reach in and change it by hand.

- Without encapsulation...

```javascript
class Counter {  
    constructor(startAt = 0){  
        this._count = startAt;  
    }

    increase(){  
        ++this._count;  
    }

    decrease(){  
        --this._count;  
    }

    get count(){  
        return this._count;  
    }  
}  
```

This will work, but `this._count` isn't really encapsulated properly. The consumer of this class could easily reach in and change the value.

- Encapsulating the component...

First, instead of making `_count` an object-level property, we'll restrict it to the scope of the constructor, and add an appropriate getter for it.


```javascript
class Counter {  
    constructor(startAt = 0){  
       let _count = startAt;  
  
       this.getCount = () => _count;  
    }  
    get count(){  
        return this.getCount();  
    }  
}  
```

The `_count` variable can not be touched outside the `class constructor`, effectively making it private.

But how do we operate on the value of `_count`?
 
We can do the same thing as `this.getCount()` because everything in the constructor has block-level access to `_count`, and hence will keep it alive while the object is being used.

```javascript
class Counter {  
    constructor(startAt = 0){  
       let _count = startAt;  
  
       this.getCount = () => _count  
  
       this.increase = () => ++_count  
       this.decrease = () => --_count
    }

    get count(){  
        return this.getCount()  
    }  
}

const counter = new Counter()  

counter.increase()
console.log(counter.count)
counter.increase()
console.log(counter.count)
```

Our variable `_count` is now truly encapsulated. You won't be able modify `_count` from outside the class itself, which is perfect for our needs.

Output is...

```
1
2
```

### Inheritance

It is a concept in which some `property` and `methods` of an `Object` is being used by another Object. Unlike most of the OOP languages where classes inherit classes, JavaScript Object `inherits Object`, i.e. certain features (property and methods) of one object can be reused by other Objects. 

- With `constructor functions`, there are steps that have to be taken 
  to properly implement inheritance.


```javascript
function Pet (name) {
    this.name = name;
}
    
function Cat (name, skills) {
    // invoke the Pet constructor function passing in our newly 
    // created this object and any required parameters
    Pet.call(this, name)

    // add additional parameters
    this.skills = skills
}

// inherit the parent's prototype functions
Cat.prototype = Object.create(Pet.prototype)

// reset our child class constructor function
Cat.prototype.constructor = Cat


const camilo = new Cat("Camilo", ["play basketball"])

console.log(camilo.name)        // Camilo
console.log(camilo.skills)      // [ 'play basketball' ]
```

- With `ES6` classes, it's not neccesary to manually copy the parent class's 
  prototype functions and reset the child class's constructor.


```javascript
// create parent class
class Pet {
    constructor (name) {
        this.name = name
    }
}

// create child class and extend our parent class
class Cat extends Pet {
    constructor (name, skills) {
        // invoke our parent constructor function passing in any required 
        // parameters
        super(name)
        this.skills = skills
    }
}

const camilo = new Cat("Camilo", ["play basketball"])

console.log(camilo.name)    // Camilo
console.log(camilo.skills)  // [ 'play basketball' ]
```


# NodeJS Challenges (Instructions) - Solving Problems  

## Object-Oriented Javascript

### Objectives

- To use `Constructor Functions`.
- To use `ES6 Classes`.

### Description

1. Objects in Javascript:

- Create two instances of `dog`, `people`, `animal` & `book` with usage of
  `constructor function` and `ES6 Syntax`.

```javascript
/*
* constructor function
*
**/

//dog

// dog instance


//people

//people instance


//animal

//animal instance


//book

//book instance


/*
* ES6 syntax
*
**/

//dog

// dog instance


//people

//people instance


//animal

//animal instance


//book

//book instance

```

2. Classes and Instances

- Use ES6 syntax to create three classes: `Passenger`, `Ride` and `Driver`.

```javascript
//Passenger class



//Ride class



//Driver class


```

- Now use these classes to create instances. First, make two instances of the 
  `Passenger` class and assign them to the variables: 


```javascript
// Two instances of the Passenger class
gussi = null
serena = null

```

- Next, make one instance of the `Driver` class and assign it to the variable, 
  `taxi`.


```Javascript
// One instance of the Driver class
taxi = null

```

- Finally, make two instances of the `Ride` class and assign them to 
  `ride_to_mexico` and `ride_home`. 


```javascript
// Two instances of the Ride class
rideToMexico = null
rideHome = null

```


- All Tests must be `True`.

```javascript
//driver code

console.log("Test 1...", gussi instanceof Passenger)
console.log("Test 2...", serena instanceof Passenger)
console.log("Test 3...", taxi instanceof Driver)
console.log("Test 4...", rideToMexico instanceof Ride)
console.log("Test 5...", rideHome instanceof Ride)
``` 

3. Bird Class:

- Use ES6 syntax to define a `Bird` class to create `Bird` instances with 
  different names and to have the following actions: `whoAmI`, `flying` and `jump`. Also, it's possible to know the bird name.


```javascript
//Bird class



```

Output example:

```javascript
const bird3 = Bird("Zulu")

console.log(bird3.jump())
// Jumping...

console.log(bird3.fly(20))
// Flying 20 mts...

console.log(bird3.fly(10))
// Flying 30 mts...

console.log(Bird.whoAmI())
// I'm a Bird
```

- All tests must be true.


```javascript
bird1 = new Bird("Billy")
bird3 = new Bird("Zulu")
bird5 = new Bird("Sally")

console.log(bird1.jump() == "Jumping...")
console.log(bird5.jump() == "Jumping...")
console.log(bird3.name == "Zulu")
console.log(Bird.whoAmI() == "I'm a Bird")
console.log(bird3.flying() == "Flying 0 mts...")
console.log(bird3.flying(20) == "Flying 20 mts...")
console.log(bird3.flying(10) == "Flying 30 mts...")
console.log(bird3.flying(10) == "Flying 40 mts...")
```


<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


> References:


1. [OOP in Javascript.](https://www.simplilearn.com/tutorials/javascript-tutorial/oop-in-javascript)

2. [Introduction OOP Javascript.](https://www.geeksforgeeks.org/introduction-object-oriented-programming-javascript/)

3. [Encapsulation in Javascript.](https://www.c-sharpcorner.com/article/encapsulation-in-javascript/)

4. [JS Prototype Inheritance without Overwiting the Base Class.](https://stackoverflow.com/questions/15098469/javascript-prototype-inheritance-without-overwriting-the-base-class)

5. [How Javascript implements OOP.](https://www.freecodecamp.org/news/how-javascript-implements-oop/)